"""
Constants for gitlab-python-runner
"""
NAME = "gitlab-python-runner"
VERSION = "12.4.5-0"
USER_AGENT = "{} {}".format(NAME, VERSION)
